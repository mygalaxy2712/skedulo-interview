public class ContactsAndAccountsWithController {
    String searchName;
    List<SearchedResult> recordsList;

    public ContactsAndAccountsWithController() {
        searchName = '';
        recordsList = new List<SearchedResult>();
    }


    public PageReference search() {
        try {
            this.recordsList.clear();
            List<List<SObject>> result = [FIND :searchName IN Name Fields RETURNING Account(Name, Id), Contact(Name, Id)];
            
            for (List<SObject> recordsList : result) {
                for (SObject record : recordsList) {
                    this.recordsList.add(new SearchedResult(record));
                }
            }
        } catch (Exception e) {
            System.debug(e);
        }

        return null;
    } 

    public List<SearchedResult> getRecordsList() {
        return recordsList;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String value) {
        searchName = value;
    }

    class SearchedResult {
        SObject record;
        
        public SearchedResult(SObject record){
            this.record = record;
        }

        public String getName() {
            return (String) record.get('Name');
        }

        public String getId() {
            return (String) record.get('Id');
        }

        public String getType() {
            return record.getSObjectType().getDescribe().getName();
        }
    }
}