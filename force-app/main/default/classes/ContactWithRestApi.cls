@RestResource(urlMapping='/contacts/*')
global with sharing class ContactWithRestApi {  
    @HttpPut
    global static void doPut() {
        PutResponseBody resBody = new PutResponseBody();

        try {
            PutRequestBody reqBody = new PutRequestBody();
            update reqBody.contactsList;

            //Write success response
            resBody.setStatus(200);
            resBody.setSuccess(true);
            resBody.setMessage('Contacts have been updated.');
        } catch (DmlException error) {
            //Write error response
            resBody.setStatus(422);
            resBody.setSuccess(false);
            resBody.setMessage(error.getDmlMessage(0));
        } catch (Exception error) {
            //Catch any exception
            System.debug(error.getMessage());
        } finally {
            resBody.sendAsJson();
        }
    }

    class PutRequestBody {
        List<Contact> contactsList;

        public PutRequestBody() {
            contactsList = new List<Contact>();
            parseBody(RestContext.request.requestBody.toString());
        }

        void parseBody(String body) {
            Map<String, Object> params = (Map<String, Object>) JSON.deserializeUntyped(body);
            List<Object> records = (List<Object>) params.get('contacts');
            
            setContactsList(records);
        }

        void setContactsList(List<Object> records) {
            for (Object record : records) {
                contactsList.add((Contact) JSON.deserializeStrict(JSON.serialize(record), Contact.class));
            }

            System.debug(contactsList);
        }
    }

    class PutResponseBody {
        public Boolean success;
        public String message;

        public PutResponseBody() {
            RestContext.response.statuscode = 400;
            success = false;
            message = 'System Errors';
        }

        public void setSuccess(Boolean value) {
            success = value;
        }

        public void setMessage(String value) {
            message = value;
        }

        public void setStatus(Integer value) {
            RestContext.response.statuscode = value;
        }

        public void sendAsJson() {
            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf(JSON.serializePretty(this));
        }
    }
}
