trigger UpdateAccount on Contact (after update, after delete) {
    
    Set<ID> accIds = new Set<ID>();
    
    if (Trigger.isUpdate) {
        for(Contact contact : Trigger.new) {
            if (contact.AccountId != null) {
                accIds.add(contact.AccountId);
            }
        }
    }

    for(Contact contact : Trigger.old) {
        if (contact.AccountId != null) {
            accIds.add(contact.AccountId);
        }
    }

    List<Account> accounts = [SELECT Id, (SELECT Id FROM Account.Contacts WHERE Active__c = TRUE) FROM Account WHERE Id IN :accIds];

    for(Account account : accounts) {
        account.Total_Contacts__c = account.Contacts.size();
    }

    update accounts;
}