# Interview Document

## Search for name of contacts and accounts 
[Demo Org](https://skedulo-d0-dev-ed.lightning.force.com/)

## Rest API
Conntected App: `Contact App` 

Method: `PUT` 

Enpoint:
> /contacts

Example Request:
```json
{
	"contacts": [
		{
			"Id": "0036D00000P05MEQAZ",
			"Active__c" : true,
			"LastName":"Update ABC contact"
		},
		{
			"Id": "0036D00000P061sQAB",
			"Active__c" : true,
			"LastName":"Demo contact is updated"
		}
	]
}
```